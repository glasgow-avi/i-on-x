package com.catch22.ionx.db;

public class Account
{
	public int id;
	public String userName, password;

	public Account(int id, String userName, String password)
	{
		this.id = id;
		this.userName = userName;
		this.password = password;
	}

	@Override
	public String toString()
	{
		return userName;
	}
}
