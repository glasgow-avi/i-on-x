package com.catch22.ionx.db;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.catch22.ionx.activities.LauncherActivity;

import java.util.ArrayList;

import static com.catch22.ionx.db.DatabaseMaster.ACCOUNTS_ID;
import static com.catch22.ionx.db.DatabaseMaster.ACCOUNTS_NUMBER;
import static com.catch22.ionx.db.DatabaseMaster.ACCOUNTS_PASSWORD;
import static com.catch22.ionx.db.DatabaseMaster.ACCOUNTS_TABLE;
import static com.catch22.ionx.db.DatabaseMaster.AttributeValuePair;

public class DatabaseHelper
{
	public static String encrypt(String string, boolean direction)
	{
		int length = string.length();

		char[] charArrayOfString = string.toCharArray();

		for(int stringIndex = 0; stringIndex < length; stringIndex++)
			charArrayOfString[stringIndex] += direction ? 41 : -41;

		char temp;
		temp = charArrayOfString[0];
		charArrayOfString[0] = charArrayOfString[charArrayOfString.length - 1];
		charArrayOfString[charArrayOfString.length - 1] = temp;

		return new String(charArrayOfString);
	}

	public static void prioritize(int id)
	{
		SQLiteDatabase db = LauncherActivity.db.getWritableDatabase();

		changeId(db, id, -1);
		changeId(db, 1, id);
		changeId(db, -1, 1);
	}

	private static void changeId(SQLiteDatabase db, int oldId, int newId)
	{
		try
		{
			db.execSQL("update " + ACCOUNTS_TABLE +
					           " set " + ACCOUNTS_ID + " = " + newId +
					           " where " + ACCOUNTS_ID + " = " + oldId);
		} catch(SQLException idDoesNotExist)
		{

		}
	}

	public static int addAccount(String userName, String password)
	{
		DatabaseMaster db = LauncherActivity.db;

		db.insertInto(ACCOUNTS_TABLE,
		              new AttributeValuePair(ACCOUNTS_PASSWORD, encrypt(password, true)),
		              new AttributeValuePair(ACCOUNTS_NUMBER, userName));

		Cursor c = db.getWritableDatabase().rawQuery("SELECT " + ACCOUNTS_ID +
				                                             " FROM " + ACCOUNTS_TABLE +
				                                             " WHERE " + ACCOUNTS_NUMBER +
				                                             " = '" + userName + "'",
		                                             null);
		int id = 0;
		while(c.moveToNext())
			id = c.getInt(0);

		return id;
	}

	public static void removeAccount(int id)
	{
		SQLiteDatabase db = LauncherActivity.db.getWritableDatabase();

		db.execSQL("delete from " + ACCOUNTS_TABLE +
				           " where " + ACCOUNTS_ID + " = " + id);
	}

	public static void editAccount(int id, String userName, String password)
	{
		SQLiteDatabase db = LauncherActivity.db.getWritableDatabase();

		db.execSQL("update " + ACCOUNTS_TABLE +
		          " set " + ACCOUNTS_NUMBER + " = '" + userName + "', " + ACCOUNTS_PASSWORD + " = '" + password + "'" +
		          " where " + ACCOUNTS_ID + " = " + id);
	}

	public static Account[] getAllAccounts()
	{
		ArrayList<Account> accounts = new ArrayList<>();

		SQLiteDatabase db = LauncherActivity.db.getWritableDatabase();

		Cursor c = db.rawQuery("SELECT " + ACCOUNTS_ID + ", " + ACCOUNTS_NUMBER + ", " + ACCOUNTS_PASSWORD +
				                       " FROM " + ACCOUNTS_TABLE +
				                       " ORDER BY " + ACCOUNTS_ID, null);

		while(c.moveToNext())
			accounts.add(new Account(c.getInt(0), c.getString(1), c.getString(2)));

		Account[] accountsArray = new Account[accounts.size()];

		for(int accountIndex = 0; accountIndex < accounts.size(); accountIndex++)
			accountsArray[accountIndex] = accounts.get(accountIndex);

		return accountsArray;
	}
}
