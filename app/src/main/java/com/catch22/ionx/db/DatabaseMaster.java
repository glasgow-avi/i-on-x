package com.catch22.ionx.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseMaster extends SQLiteOpenHelper
{
	protected static class Column
	{
		public String name;
		public String dataType;
		public String constraints;

		public Column(String name, String dataType)
		{
			this.name = name;
			this.dataType = dataType;
			this.constraints = "";
		}

		public Column(String name, String dataType, String constraints)
		{
			this.name = name;
			this.dataType = dataType;
			this.constraints = constraints;
		}
	}

	protected static class AttributeValuePair
	{
		public String attribute;
		public Object value;

		public AttributeValuePair(String attribute, Object value)
		{
			this.attribute = attribute;
			this.value = value;
		}
	}

	public static final String DATABASE_NAME = "ionx.db";
	public static String ACCOUNTS_TABLE = "Accounts",
			ACCOUNTS_ID = "UniqueId",
			ACCOUNTS_NUMBER = "Id",
			ACCOUNTS_PASSWORD = "Password";

	public DatabaseMaster(Context context)
	{
		super(context, DATABASE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		onUpgrade(db, 1, 1);

		createTable(db, ACCOUNTS_TABLE,
		            new Column(ACCOUNTS_ID, "integer", "primary key"),
		            new Column(ACCOUNTS_NUMBER, "text"),
		            new Column(ACCOUNTS_PASSWORD, "text"));

		Log.d("SQL", "Table created");
	}

	private final void createTable(SQLiteDatabase db, String tableName, Column... columns)
	{
		String query = "create table " + tableName + "(";
		for(Column column : columns)
			query += column.name + " " + column.dataType + " " + column.constraints + ", ";

		query = query.substring(0, query.length() - 2);

		query += ");";

		Log.d("SQL", "Attempting query " + query);
		db.execSQL(query);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		db.execSQL("DROP TABLE IF EXISTS " + ACCOUNTS_TABLE);;
	}

	public void insertInto(String tableName, AttributeValuePair... values)
	{
		ContentValues contentValues = new ContentValues();

		for(AttributeValuePair value : values)
			if(value.value instanceof String)
				contentValues.put(value.attribute, (String) value.value);
			else
				contentValues.put(value.attribute, (Integer) value.value);

		getWritableDatabase().insert(tableName, null, contentValues);
	}

	public void deleteAllEntries()
	{
		getWritableDatabase().execSQL("delete from " + ACCOUNTS_TABLE);
	}
}