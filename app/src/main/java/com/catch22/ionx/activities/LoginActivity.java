package com.catch22.ionx.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.catch22.ionx.R;
import com.catch22.ionx.web.IonWebViewClient;
import com.catch22.ionx.web.Urls;

public class LoginActivity extends AppCompatActivity
{
    private WebView ionWebView;
	private static IonWebViewClient webViewClient = new IonWebViewClient();

    private final void initializeWebView()
    {
        ionWebView = (WebView)findViewById(R.id.webView);
	    WebSettings webSettings = ionWebView.getSettings();
	    webSettings.setJavaScriptEnabled(true);
        ionWebView.loadUrl(Urls.url);
        webSettings.setDomStorageEnabled(true);

        ionWebView.setWebViewClient(webViewClient);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initializeWebView();
    }
}