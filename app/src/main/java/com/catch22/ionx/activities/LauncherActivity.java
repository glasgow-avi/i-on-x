package com.catch22.ionx.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.catch22.ionx.R;
import com.catch22.ionx.db.Account;
import com.catch22.ionx.db.DatabaseHelper;
import com.catch22.ionx.db.DatabaseMaster;

public class LauncherActivity extends AppCompatActivity
{
	private FloatingActionButton addAccountButton;
	private static ListView listView;

	public static DatabaseMaster db;

	public static Account currentlySelectedAccount;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if(db == null)
			db = new DatabaseMaster(this);

		setContentView(R.layout.activity_launcher);

		final Button contextMenuCreateButton = (Button)findViewById(R.id.contextMenuCreatorButton);
		registerForContextMenu(contextMenuCreateButton);

		listView = (ListView)findViewById(R.id.listView);
		addAccountButton = (FloatingActionButton)findViewById(R.id.addAccountButton);
		ActivityFactory.setNavigation(addAccountButton, LauncherActivity.this, AccountAdderPopup.class);

		final ListAdapter listAdapter = new ArrayAdapter<>(this,
		                                                   R.layout.support_simple_spinner_dropdown_item,
		                                                   DatabaseHelper.getAllAccounts());

		listView.setAdapter(listAdapter);
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l)
			{
				currentlySelectedAccount = (Account)adapterView.getItemAtPosition(i);
				contextMenuCreateButton.performLongClick();
				return true;
			}
		});

		addAccountButton.setLongClickable(true);
		addAccountButton.setOnLongClickListener(new View.OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View view)
			{
				ActivityFactory.navigate(LauncherActivity.this, LoginActivity.class);
				return true;
			}
		});
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
	{
		super.onCreateContextMenu(menu, v, menuInfo);

		try
		{
			MenuInflater menuInflater = getMenuInflater();
			menuInflater.inflate(R.menu.context_menu, menu);
		} catch(Throwable e)
		{
			Log.d("Context Menu", "Failed to create");
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
			case R.id.popup_menu_set_as_priority:
				DatabaseHelper.prioritize(currentlySelectedAccount.id);
				ActivityFactory.navigate(LauncherActivity.this, LauncherActivity.class);
				break;
			case R.id.popup_menu_remove:
				DatabaseHelper.removeAccount(currentlySelectedAccount.id);
				ActivityFactory.navigate(LauncherActivity.this, LauncherActivity.class);
				break;
			case R.id.popup_menu_edit:
				AccountAdderPopup.editing = true;
				ActivityFactory.navigate(LauncherActivity.this, AccountAdderPopup.class);
		}
		return false;
	}
}
