package com.catch22.ionx.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.catch22.ionx.R;
import com.catch22.ionx.db.DatabaseHelper;

public class AccountAdderPopup extends AppCompatActivity
{
	private EditText userNameEditText, passwordEditText;
	private CheckBox showPasswordCheckBox;
	private Button doneButton;

	public static boolean editing = false;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_adder_popup);

		userNameEditText = (EditText)findViewById(R.id.userNameEditText);
		passwordEditText = (EditText)findViewById(R.id.passwordEditText);
		showPasswordCheckBox = (CheckBox)findViewById(R.id.showPasswordCheckBox);
		doneButton = (Button)findViewById(R.id.doneButton);

		passwordEditText.setInputType(showPasswordCheckBox.isChecked() ?
				                              InputType.TYPE_TEXT_VARIATION_PASSWORD :
				                              129);

		showPasswordCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				passwordEditText.setInputType(showPasswordCheckBox.isChecked() ?
						                              InputType.TYPE_TEXT_VARIATION_PASSWORD :
						                              129);
			}
		});

		if(editing)
			userNameEditText.setText(LauncherActivity.currentlySelectedAccount.userName);

		doneButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				String userName = userNameEditText.getText().toString();
				String password = passwordEditText.getText().toString();

				//TODO regex
				if("".equals(userName) || "".equals(password))
				{
					Toast.makeText(getBaseContext(), "Check your input format", Toast.LENGTH_SHORT).show();
					return;
				}

				if(editing)
					DatabaseHelper.editAccount(LauncherActivity.currentlySelectedAccount.id, userName, password);
				else
					DatabaseHelper.addAccount(userName, password);

				editing = false;
				ActivityFactory.navigate(AccountAdderPopup.this, LauncherActivity.class);
			}
		});
	}
}
