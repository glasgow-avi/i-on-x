package com.catch22.ionx.web;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkConnectionManger
{
	public static final boolean isLoggedIn()
	{
		try
		{
			HttpURLConnection urlC = (HttpURLConnection)(new URL(Urls.pingTestUrl).openConnection());

			urlC.setRequestProperty("User-Agent", "Test");
			urlC.setRequestProperty("Connection", "close");
			urlC.setConnectTimeout(2000);
			urlC.connect();
			return (urlC.getResponseCode() == 204 && urlC.getContentLength() == 0);
		} catch (IOException e)
		{

		}
		return false;
	}
}
