package com.catch22.ionx.web;

import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.catch22.ionx.db.Account;
import com.catch22.ionx.db.DatabaseHelper;

public class IonWebViewClient extends WebViewClient
{
	private static final String LOG_TAG = "LOGIN_MANAGER";
	private boolean loggedIn = false;
	private static int loginAttempt = 0;

	private void injectLoginQuery(String username, String password, WebView v)
	{
		String script = "javascript: {var user = document.getElementsByName('username')[0].value = '" + username + "';" +
				"var pass = document.getElementsByName('password')[0].value = '" + DatabaseHelper.encrypt(password, false) + "';" +
				"var frm = document.getElementsByName('clientloginform')[0].submit(); };";

		v.loadUrl(script);
		loginAttempt++;
	}

	@Override
	public void onPageFinished(WebView v, String url)
	{
		Thread networkChecker = new Thread(new Runnable(){
			@Override
			public void run()
			{
				loggedIn = NetworkConnectionManger.isLoggedIn();
			}
		});
		networkChecker.start();

		long startTime = System.currentTimeMillis();
		while(networkChecker.isAlive())
			if(System.currentTimeMillis() - startTime > 3000)
			{
				Log.d(LOG_TAG, "Timeout");
				break;
			}

		Account[] accounts = DatabaseHelper.getAllAccounts();

		if(!loggedIn && loginAttempt < accounts.length)
		{
			injectLoginQuery(accounts[loginAttempt].userName,
			                 accounts[loginAttempt].password,
			                 v); //enter the login details
		}
		else
		{
			Log.d(LOG_TAG, "Skipped log in.");
			if(loginAttempt == accounts.length)
				Log.d(LOG_TAG, "Exhausted Login attempts.");
			loginAttempt = 0;
		}
	}
}